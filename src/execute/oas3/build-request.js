// This function runs after the common function,
// `src/execute/index.js#buildRequest`
import assign from 'lodash/assign';
import get from 'lodash/get';
import btoa from 'btoa';

export default function buildRequest(options, req) {
  const { operation, requestBody, securities, spec, attachContentTypeForEmptyPayload } = options;

  let { requestContentType } = options;

  req = applySecurities({
    request: req,
    securities,
    operation,
    spec,
  });

  const requestBodyDef = operation.requestBody || {};
  let requestBodyMediaTypes = Object.keys(requestBodyDef.content || {});
  const isExplicitContentTypeValid =
    requestContentType && requestBodyMediaTypes.indexOf(requestContentType) > -1;
  
  if(requestContentType){
    if((requestContentType == 'password') || (requestContentType=='selectAccount') || (requestContentType=='refreshToken') || 
      (requestContentType=='applicationId') || (requestContentType=='accountUserId') || (requestContentType=='applicationuserId')){      
        requestContentType = 'application/json';
        requestBodyMediaTypes = ['application/json'];    
    }
  }

  // for OAS3: set the Content-Type
  if (requestBody || attachContentTypeForEmptyPayload) {
    // does the passed requestContentType appear in the requestBody definition?

    if (requestContentType && isExplicitContentTypeValid) {
      req.headers['Content-Type'] = requestContentType;
    } else if (!requestContentType) {
      const firstMediaType = requestBodyMediaTypes[0];
      if (firstMediaType) {
        req.headers['Content-Type'] = firstMediaType;
        requestContentType = firstMediaType;
      }
    }
  } else if (requestContentType && isExplicitContentTypeValid) {
    req.headers['Content-Type'] = requestContentType;
  }

  //MindSphere changes
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  };
  var crfs = getCookie('XSRF-TOKEN');

  if(crfs){    
    req.headers['x-xsrf-token'] = crfs;    
  }
  //MindSphere changes

  // for OAS3: add requestBody to request
  if (requestBody) {
    if (requestContentType) {
      if (requestBodyMediaTypes.indexOf(requestContentType) > -1) {
        // only attach body if the requestBody has a definition for the
        // contentType that has been explicitly set
        if (
          requestContentType === 'application/x-www-form-urlencoded' ||
          requestContentType === 'multipart/form-data'
        ) {
          if (typeof requestBody === 'object') {
            const encoding = (requestBodyDef.content[requestContentType] || {}).encoding || {};

            req.form = {};
            Object.keys(requestBody).forEach((k) => {
              req.form[k] = {
                value: requestBody[k],
                encoding: encoding[k] || {},
              };
            });
          } else {
            req.form = requestBody;
          }
        } else {
          req.body = requestBody;
        }
      }
    } else {
      req.body = requestBody;
    }
  }

  return req;
}

// Add security values, to operations - that declare their need on them
// Adapted from the Swagger2 implementation
export function applySecurities({ request, securities = {}, operation = {}, spec }) {
  const result = assign({}, request);
  const { authorized = {} } = securities;
  const security = operation.security || spec.security || [];
  const isAuthorized = authorized && !!Object.keys(authorized).length;
  const securityDef = get(spec, ['components', 'securitySchemes']) || {};

  result.headers = result.headers || {};
  result.query = result.query || {};

  if (
    !Object.keys(securities).length ||
    !isAuthorized ||
    !security ||
    (Array.isArray(operation.security) && !operation.security.length)
  ) {
    return request;
  }

  security.forEach((securityObj) => {
    Object.keys(securityObj).forEach((key) => {
      const auth = authorized[key];
      const schema = securityDef[key];

      if (!auth) {
        return;
      }

      const value = auth.value || auth;
      const { type } = schema;

      if (auth) {
        if (type === 'apiKey') {
          if (schema.in === 'query') {
            result.query[schema.name] = value;
          }
          if (schema.in === 'header') {
            result.headers[schema.name] = value;
          }
          if (schema.in === 'cookie') {
            result.cookies[schema.name] = value;
          }
        } else if (type === 'http') {
          if (/^basic$/i.test(schema.scheme)) {
            const username = value.username || '';
            const password = value.password || '';
            const encoded = btoa(`${username}:${password}`);
            result.headers.Authorization = `Basic ${encoded}`;
          }

          if (/^bearer$/i.test(schema.scheme)) {
            result.headers.Authorization = `Bearer ${value}`;
          }
        } else if (type === 'oauth2' || type === 'openIdConnect') {
          const token = auth.token || {};
          let tokenName = schema['x-tokenName'] || 'access_token';
          let tokenValue = token[tokenName];
          let tokenType = token.token_type;
          if(localStorage.getItem('oauth2Token')){
            tokenValue = localStorage.getItem('oauth2Token');
          }
          if (!tokenType || tokenType.toLowerCase() === 'bearer') {
            tokenType = 'Bearer';
          }

          result.headers.Authorization = `${tokenType} ${tokenValue}`;
        }
      }
    });
  });

  return result;
}
