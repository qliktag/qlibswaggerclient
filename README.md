# QlibSwaggerUiClient

## What is this repository for?
QlibSwaggerUiClient is created inorder to support changes done on swagger-ui
- queryString - passing list of query paramter which does not need seprate entry in parameters section 
- XSRF-TOKEN token changes to send cookie x-xsrf-token header 
- whitelist content-type,accept and authorization token 
- show different option for oauth api i.e diff grantType

## Setting up
* Install NVM version - 0.33.8
* Install Node version using nvm - 10.16.3

### Steps
- npm install
- npm run build

above steps will output build files in dist/,  es/ and lib/ folder,

copy es/ folder after build and replace the folder in QlibSwaggerUi project node_modules swagger-client es/ folder for verfying your changes before committing any code into this repository

these changes will be used in QlibSwaggerUi
