import _concatInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/concat";
import _Array$isArray from "@babel/runtime-corejs3/core-js-stable/array/is-array";
import _forEachInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/for-each";
import _typeof from "@babel/runtime-corejs3/helpers/typeof";
import _indexOfInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/index-of";
import _Object$keys from "@babel/runtime-corejs3/core-js-stable/object/keys";
// This function runs after the common function,
// `src/execute/index.js#buildRequest`
import assign from 'lodash/assign';
import get from 'lodash/get';
import btoa from 'btoa';
export default function buildRequest(options, req) {
  var operation = options.operation,
      requestBody = options.requestBody,
      securities = options.securities,
      spec = options.spec,
      attachContentTypeForEmptyPayload = options.attachContentTypeForEmptyPayload;
  var requestContentType = options.requestContentType;
  req = applySecurities({
    request: req,
    securities: securities,
    operation: operation,
    spec: spec
  });
  var requestBodyDef = operation.requestBody || {};

  var requestBodyMediaTypes = _Object$keys(requestBodyDef.content || {});

  var isExplicitContentTypeValid = requestContentType && _indexOfInstanceProperty(requestBodyMediaTypes).call(requestBodyMediaTypes, requestContentType) > -1;

  if (requestContentType) {
    if (requestContentType == 'password' || requestContentType == 'selectAccount' || requestContentType == 'refreshToken' || requestContentType == 'applicationId' || requestContentType == 'accountUserId' || requestContentType == 'applicationuserId') {
      requestContentType = 'application/json';
      requestBodyMediaTypes = ['application/json'];
    }
  } // for OAS3: set the Content-Type


  if (requestBody || attachContentTypeForEmptyPayload) {
    // does the passed requestContentType appear in the requestBody definition?
    if (requestContentType && isExplicitContentTypeValid) {
      req.headers['Content-Type'] = requestContentType;
    } else if (!requestContentType) {
      var firstMediaType = requestBodyMediaTypes[0];

      if (firstMediaType) {
        req.headers['Content-Type'] = firstMediaType;
        requestContentType = firstMediaType;
      }
    }
  } else if (requestContentType && isExplicitContentTypeValid) {
    req.headers['Content-Type'] = requestContentType;
  } //MindSphere changes


  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];

      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }

      if (_indexOfInstanceProperty(c).call(c, name) == 0) {
        return c.substring(name.length, c.length);
      }
    }

    return "";
  }

  ;
  var crfs = getCookie('XSRF-TOKEN');

  if (crfs) {
    req.headers['x-xsrf-token'] = crfs;
  } //MindSphere changes
  // for OAS3: add requestBody to request


  if (requestBody) {
    if (requestContentType) {
      if (_indexOfInstanceProperty(requestBodyMediaTypes).call(requestBodyMediaTypes, requestContentType) > -1) {
        // only attach body if the requestBody has a definition for the
        // contentType that has been explicitly set
        if (requestContentType === 'application/x-www-form-urlencoded' || requestContentType === 'multipart/form-data') {
          if (_typeof(requestBody) === 'object') {
            var _context;

            var encoding = (requestBodyDef.content[requestContentType] || {}).encoding || {};
            req.form = {};

            _forEachInstanceProperty(_context = _Object$keys(requestBody)).call(_context, function (k) {
              req.form[k] = {
                value: requestBody[k],
                encoding: encoding[k] || {}
              };
            });
          } else {
            req.form = requestBody;
          }
        } else {
          req.body = requestBody;
        }
      }
    } else {
      req.body = requestBody;
    }
  }

  return req;
} // Add security values, to operations - that declare their need on them
// Adapted from the Swagger2 implementation

export function applySecurities(_ref) {
  var request = _ref.request,
      _ref$securities = _ref.securities,
      securities = _ref$securities === void 0 ? {} : _ref$securities,
      _ref$operation = _ref.operation,
      operation = _ref$operation === void 0 ? {} : _ref$operation,
      spec = _ref.spec;
  var result = assign({}, request);
  var _securities$authorize = securities.authorized,
      authorized = _securities$authorize === void 0 ? {} : _securities$authorize;
  var security = operation.security || spec.security || [];
  var isAuthorized = authorized && !!_Object$keys(authorized).length;
  var securityDef = get(spec, ['components', 'securitySchemes']) || {};
  result.headers = result.headers || {};
  result.query = result.query || {};

  if (!_Object$keys(securities).length || !isAuthorized || !security || _Array$isArray(operation.security) && !operation.security.length) {
    return request;
  }

  _forEachInstanceProperty(security).call(security, function (securityObj) {
    var _context2;

    _forEachInstanceProperty(_context2 = _Object$keys(securityObj)).call(_context2, function (key) {
      var auth = authorized[key];
      var schema = securityDef[key];

      if (!auth) {
        return;
      }

      var value = auth.value || auth;
      var type = schema.type;

      if (auth) {
        if (type === 'apiKey') {
          if (schema["in"] === 'query') {
            result.query[schema.name] = value;
          }

          if (schema["in"] === 'header') {
            result.headers[schema.name] = value;
          }

          if (schema["in"] === 'cookie') {
            result.cookies[schema.name] = value;
          }
        } else if (type === 'http') {
          if (/^basic$/i.test(schema.scheme)) {
            var _context3;

            var username = value.username || '';
            var password = value.password || '';
            var encoded = btoa(_concatInstanceProperty(_context3 = "".concat(username, ":")).call(_context3, password));
            result.headers.Authorization = "Basic ".concat(encoded);
          }

          if (/^bearer$/i.test(schema.scheme)) {
            result.headers.Authorization = "Bearer ".concat(value);
          }
        } else if (type === 'oauth2' || type === 'openIdConnect') {
          var _context4;

          var token = auth.token || {};
          var tokenName = schema['x-tokenName'] || 'access_token';
          var tokenValue = token[tokenName];
          var tokenType = token.token_type;

          if (localStorage.getItem('oauth2Token')) {
            tokenValue = localStorage.getItem('oauth2Token');
          }

          if (!tokenType || tokenType.toLowerCase() === 'bearer') {
            tokenType = 'Bearer';
          }

          result.headers.Authorization = _concatInstanceProperty(_context4 = "".concat(tokenType, " ")).call(_context4, tokenValue);
        }
      }
    });
  });

  return result;
}