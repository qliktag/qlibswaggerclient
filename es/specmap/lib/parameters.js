import _forEachInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/for-each";
import _objectSpread from "@babel/runtime-corejs3/helpers/objectSpread2";
import _sliceInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/slice";
import _Object$assign from "@babel/runtime-corejs3/core-js-stable/object/assign";
import _Array$isArray from "@babel/runtime-corejs3/core-js-stable/array/is-array";
import lib from '.';
export default {
  key: 'parameters',
  plugin: function plugin(parameters, key, fullPath, specmap) {
    if (_Array$isArray(parameters) && parameters.length) {
      var val = _Object$assign([], parameters);

      var opPath = _sliceInstanceProperty(fullPath).call(fullPath, 0, -1);

      var op = _objectSpread({}, lib.getIn(specmap.spec, opPath));

      _forEachInstanceProperty(parameters).call(parameters, function (param, i) {
        try {
          val[i]["default"] = specmap.parameterMacro(op, param);
        } catch (e) {
          var err = new Error(e);
          err.fullPath = fullPath;
          return err;
        }

        return undefined;
      });

      return lib.replace(fullPath, val);
    }

    return lib.replace(fullPath, parameters);
  }
};