import _slicedToArray from "@babel/runtime-corejs3/helpers/slicedToArray";
import _includesInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/includes";
import _forEachInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/for-each";
import _concatInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/concat";
import _toConsumableArray from "@babel/runtime-corejs3/helpers/toConsumableArray";
import _someInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/some";
import _indexOfInstanceProperty from "@babel/runtime-corejs3/core-js-stable/instance/index-of";
import traverse from 'traverse';
import URL from 'url';
import isString from 'lodash/isString'; // This will match if the direct parent's key exactly matches an item.

var freelyNamedKeyParents = ['properties']; // This will match if the grandparent's key exactly matches an item.
// NOTE that this is for finding non-free paths!

var nonFreelyNamedKeyGrandparents = ['properties']; // This will match if the joined parent path exactly matches an item.
//
// This is mostly useful for filtering out root-level reusable item names,
// for example `["definitions", "$ref"]`

var freelyNamedPaths = [// Swagger 2.0
'definitions', 'parameters', 'responses', 'securityDefinitions', // OpenAPI 3.0
'components/schemas', 'components/responses', 'components/parameters', 'components/securitySchemes']; // This will match if any of these items are substrings of the joined
// parent path.
//
// Warning! These are powerful. Beware of edge cases.

var freelyNamedAncestors = ['schema/example', 'items/example'];
export function isFreelyNamed(parentPath) {
  var parentKey = parentPath[parentPath.length - 1];
  var grandparentKey = parentPath[parentPath.length - 2];
  var parentStr = parentPath.join('/');
  return (// eslint-disable-next-line max-len
    _indexOfInstanceProperty(freelyNamedKeyParents).call(freelyNamedKeyParents, parentKey) > -1 && _indexOfInstanceProperty(nonFreelyNamedKeyGrandparents).call(nonFreelyNamedKeyGrandparents, grandparentKey) === -1 || _indexOfInstanceProperty(freelyNamedPaths).call(freelyNamedPaths, parentStr) > -1 || _someInstanceProperty(freelyNamedAncestors).call(freelyNamedAncestors, function (el) {
      return _indexOfInstanceProperty(parentStr).call(parentStr, el) > -1;
    })
  );
}
export function generateAbsoluteRefPatches(obj, basePath) {
  var _context2;

  var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
      specmap = _ref.specmap,
      _ref$getBaseUrlForNod = _ref.getBaseUrlForNodePath,
      getBaseUrlForNodePath = _ref$getBaseUrlForNod === void 0 ? function (path) {
    var _context;

    return specmap.getContext(_concatInstanceProperty(_context = []).call(_context, _toConsumableArray(basePath), _toConsumableArray(path))).baseDoc;
  } : _ref$getBaseUrlForNod,
      _ref$targetKeys = _ref.targetKeys,
      targetKeys = _ref$targetKeys === void 0 ? ['$ref', '$$ref'] : _ref$targetKeys;

  var patches = [];

  _forEachInstanceProperty(_context2 = traverse(obj)).call(_context2, function callback() {
    if (_includesInstanceProperty(targetKeys).call(targetKeys, this.key) && isString(this.node)) {
      var nodePath = this.path; // this node's path, relative to `obj`

      var fullPath = _concatInstanceProperty(basePath).call(basePath, this.path);

      var absolutifiedRefValue = absolutifyPointer(this.node, getBaseUrlForNodePath(nodePath));
      patches.push(specmap.replace(fullPath, absolutifiedRefValue));
    }
  });

  return patches;
}
export function absolutifyPointer(pointer, baseUrl) {
  var _context3;

  var _pointer$split = pointer.split('#'),
      _pointer$split2 = _slicedToArray(_pointer$split, 2),
      urlPart = _pointer$split2[0],
      fragmentPart = _pointer$split2[1];

  var newRefUrlPart = URL.resolve(urlPart || '', baseUrl || '');
  return fragmentPart ? _concatInstanceProperty(_context3 = "".concat(newRefUrlPart, "#")).call(_context3, fragmentPart) : newRefUrlPart;
}